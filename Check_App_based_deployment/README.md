# Check App for device based deployment
A simple script to check an app to see if it is enabled for device based deployment through an MDM

### Requirements
You will need to install [rest-client](https://github.com/rest-client/rest-client) to get use this script
##### Install:
`gem install rest-client`
### Usage
To run this script all you need is an appId which you can get by looking at the url of the app.<br />
e.g. https://itunes.apple.com/au/app/canvas-by-instructure/id480883488?mt=8<br />
where 459248037 is the id of the app.
```
ruby apple_app_device_based_licensing.rb <appId>
```
### Comments
If you have any feedback or want to improve the app, please go right ahead.
