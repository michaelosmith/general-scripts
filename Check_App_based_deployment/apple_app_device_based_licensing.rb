#!/usr/bin/ruby

##
# Author: Michael Smith
#
# This script will contact Apple API and determine if the app is able to be
# deployed via device based vpp, based on a appId provided.
#
# Usage: ruby apple_app_device_based_licensing.rb <appId>


require 'rest-client'

# Define some colours to use in output text.
class String
def black;          "\e[30m#{self}\e[0m" end
def red;            "\e[31m#{self}\e[0m" end
def green;          "\e[32m#{self}\e[0m" end
def brown;          "\e[33m#{self}\e[0m" end
def blue;           "\e[34m#{self}\e[0m" end
def magenta;        "\e[35m#{self}\e[0m" end
def cyan;           "\e[36m#{self}\e[0m" end
def gray;           "\e[37m#{self}\e[0m" end

def bg_black;       "\e[40m#{self}\e[0m" end
def bg_red;         "\e[41m#{self}\e[0m" end
def bg_green;       "\e[42m#{self}\e[0m" end
def bg_brown;       "\e[43m#{self}\e[0m" end
def bg_blue;        "\e[44m#{self}\e[0m" end
def bg_magenta;     "\e[45m#{self}\e[0m" end
def bg_cyan;        "\e[46m#{self}\e[0m" end
def bg_gray;        "\e[47m#{self}\e[0m" end

def bold;           "\e[1m#{self}\e[22m" end
def italic;         "\e[3m#{self}\e[23m" end
def underline;      "\e[4m#{self}\e[24m" end
def blink;          "\e[5m#{self}\e[25m" end
def reverse_color;  "\e[7m#{self}\e[27m" end
end


if (ARGV.length != 1)
	puts "Usage: apple_app_device_based_licensing.rb <appId>"
	abort
end

# Get the appId
app_id = ARGV[0]

APP_DETAILS_URL = "https://itunes.apple.com/au/lookup"

app_json = JSON.parse(RestClient.get APP_DETAILS_URL, {:params => {:id => app_id}}{|response, request, result| response})

begin
  app_test = app_json["results"][0]["screenshotUrls"]
rescue
  abort "App id #{app_id} is not valid.".bold.red
end

app = app_json["results"][0]

if app["isVppDeviceBasedLicensingEnabled"]
  device_license_deploy = "This app is enabled for deployment to devices.".bold.green
else
  device_license_deploy = "This app cannot be deployed using device based licensing".bold.red
end

# Output
puts " "
puts "App Name: ".bold + app["trackCensoredName"]
puts "Age restrictions: ".bold + app["contentAdvisoryRating"]
puts device_license_deploy
puts ""
