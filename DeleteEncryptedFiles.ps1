## CryptoLocker File Removal Script - Version 2.0 by Daniel Maguire 2015-09-16
## Released under the intention of GPL v3.0 - if you improve this script, make it available!
##
## Re-written in response to muppet(s) CryptoLocking their network drives and scattering .encrypted files all over the place
## See: https://en.wikipedia.org/wiki/CryptoLocker
##
## As always, isolate and remove offending machines from the network first and deal with them seperately
## Run this script to remove the damaged files, then restore ALL files from backup and SKIP any existing files
## This should mean you end up with full restore of the encrypted items while leaving everything else alone
## 
## Define the root directory to be scanned as $Path, save and run the script from an elevated PowerShell prompt
## A log file will be place in the root directory with a list of all files identified and removed

$Path = "C:\Test"

$Dirs = Get-ChildItem -Path $Path -Recurse | Where-Object {$_.PsIsContainer}

Foreach ($Dir in $Dirs) {
    
    $Files = Get-ChildItem -Path $Dir.FullName | Where-Object {-not $_.PsIsContainer -and $_.name -like "*.encrypted"}
    Out-File -FilePath $Path\DeletedFiles.log -Append -InputObject $Files
    
    Foreach ($File in $Files) {
        
        $FilePath = Join-Path $Dir.FullName $File
        Remove-Item $FilePath -Force
        Write-Host "Deleting File: $FilePath"
    } 
}
