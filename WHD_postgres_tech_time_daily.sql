SELECT to_char(tech_note_date::timestamptz AT TIME ZONE 'Australia/Sydney', 'YYYY-MM-DD') , sum(billing_minutes) , concat(tech.first_name, ' ', tech.last_name)
FROM public.tech_note left join public.tech on public.tech_note.technician_id = public.tech.client_id
Where tech_note_date BETWEEN (LOCALTIMESTAMP::timestamptz AT TIME ZONE 'Australia/Sydney') - INTERVAL '14 days' AND (LOCALTIMESTAMP::timestamptz AT TIME ZONE 'Australia/Sydney') and technician_id in (Select client_id from tech)
Group by to_char(tech_note.tech_note_date::timestamptz AT TIME ZONE 'Australia/Sydney', 'YYYY-MM-DD'), tech.last_name, tech.first_name
Order by to_char(tech_note.tech_note_date::timestamptz AT TIME ZONE 'Australia/Sydney', 'YYYY-MM-DD') desc
