repadmin /showrepl * /bysrc /bydest

# /a means "all domain controllers", /v means "verbose logging" and /c means "comprehensive set of tests."
dcdiag /a /v /c

# run the six default DNS subtests against all DCs
DCDIAG /TEST:DNS /V /E
