
function Backup-Eventlog ($logFileName, $path = "C:\dancrai\eventLogs\") {
  # Config
  #$logFileName = "Application" # Add Name of the Logfile (System, Application, etc)
  #$path = "C:\dancrai\eventLogs\" # Add Path, needs to end with a backsplash

  # do not edit
  $exportFileName = $logFileName + (get-date -f yyyyMMdd) + ".evt"
  $logFile = Get-WmiObject Win32_NTEventlogFile | Where-Object {$_.logfilename -eq $logFileName}
  $logFile.backupeventlog($path + $exportFileName)


  # Deletes all .evt logfiles in $path
  # Be careful, this script removes all files with the extension .evt not just the selfcreated logfiles
  $Daysback = "-60"

  $CurrentDate = Get-Date
  $DatetoDelete = $CurrentDate.AddDays($Daysback)
  Get-ChildItem $Path | Where-Object { ($_.LastWriteTime -lt $DatetoDelete) -and ($_.Extension -eq ".evt") } | Remove-Item
  Clear-Eventlog -LogName $logFileName
}

Backup-Eventlog -logFileName "Application"
Backup-Eventlog -logFileName "System"
Backup-Eventlog -logFileName "Security"
