#!/usr/bin/ruby

##
# Author: Michael Smith
#
# This script will contact Dell API and pull down information from the API
# based on a serviceTag provided.
#
# Usage: ruby dell_warranty_v2.rb <servicetag>

require 'rest-client'

if (ARGV.length != 1)
	puts "Usage: dell_warranty_v2.rb <service tag>"
	abort
end

# Get the service tag
service_tag = ARGV[0]

WARRANTY_CHECK_API_URL = "https://api.dell.com/support/v2/assetinfo/warranty/tags.json"

API_KEY = "849e027f476027a394edd656eaef4842"

asset_json = JSON.parse(RestClient.get WARRANTY_CHECK_API_URL, {:params => {:svctags => service_tag, :apikey => API_KEY}})

begin
  dell_asset = asset_json["GetAssetWarrantyResponse"]["GetAssetWarrantyResult"]["Response"]["DellAsset"]
  dell_warranties = asset_json["GetAssetWarrantyResponse"]["GetAssetWarrantyResult"]["Response"]["DellAsset"]["Warranties"]["Warranty"]
rescue
  abort "Service tag #{service_tag} not found"
end

# Output Warrany information
puts "\n\033[1mModel:\033[0m " + dell_asset["MachineDescription"]
puts "\033[1mService Tag:\033[0m " + dell_asset["ServiceTag"]
puts "\n"
puts "\033[1mWarranty Information:\033[0m"
puts "\n"
dell_warranties.each do |w|
  puts "\033[1mService Description:\033[0m " + w["ServiceLevelDescription"]
  puts "\033[1mStart Date:\033[0m " + w["StartDate"]
  puts "\033[1mEnd Date:\033[0m   " + w["EndDate"]
  puts "\n"
end
puts "\n"
