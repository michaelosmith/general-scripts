@echo off

REM Add registry keys to disable and reset CSC Cache
REG ADD "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\CSC" /v Start /t REG_DWORD /d 4 /f
REG ADD "HKLM\System\CurrentControlSet\Services\CSC\Parameters" /v FormatDatabase /t REG_DWORD /d 1 /f

REM Restart computer
SHUTDOWN /r /t 5 /c "Restart to fix CSC Cache issues" /d p:2:4
