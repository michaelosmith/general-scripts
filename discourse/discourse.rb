#!/usr/bin/ruby

require 'json'

file = File.read('canvas.json')
data_hash = JSON.parse(file)

data_hash.each do |t|
  puts "Title: " + t['title']
  puts "Body:"
  puts t['message']
  puts "==================="
  puts "\n"
end

puts data_hash.count
