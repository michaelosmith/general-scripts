#!/usr/bin/python2
'''
An example script do download all of the Netbox logs in an automated manner.

This should be run daily to get the previous days logs.

This must be run using Python 2.7.
This is already installed on most operating systems.
If running Windows, it can be installed using this installer: http://python.org/ftp/python/2.7.5/python-2.7.5.msi
'''

# Include username and password here
BASE_URL = 'https://tech:tech@covenant.safenetbox.biz:4433/'

# Directory to put files into. This needs to exist
OUTPUT_DIR = r'/Users/michaels/Documents/logs/netbox'

# Set to 0 for fewer records (Verbosity Normal).
VERBOSE = -1

# Set to True to overwrite any existing files. If False any existing files will be appended to.
REPLACE = False

# -------------- No configuration is required below this line ---------------------
#

# Notes on required variables when saving a text only log
#   logtype: This is the type of log to download, this script will automatically download all log types, you can see the available modes in the web UI, as the values for each Log Type option
#   custom-starttime: The start time to view the logs, in ISO format
#   custom-endtime: The start time to view the logs, in ISO format
#   applyfilter: The filter to apply, normally empty
#   fastfilter: Only applicable if an applyfilter is applied, typically 0
#   verbosity: The verbosity to display, -1 or 0 are allowed.
#   mode: This should also be "history"
#   wrap: This must be 0
#   frame: This must be "bottom"
#   text: If set, then the output will be in plain test, not HTML

PATH = '/basic/logview/logview?mode=history&logtype=%s&applyfilter=&custom-starttime=%s&custom-endtime=%s&fastfilter=0&verbosity=%s&wrap=0&frame=bottom&text=1'

VIEW_PATH = '/basic/logview/'

import datetime
import urllib
import os.path
import re
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

def availablelogs():
    # All logs available on the site
    url = BASE_URL + VIEW_PATH
    data = urllib.urlopen(url).read()
    optionsre = re.compile(r'<option value=\"(.+?_logs)\">')
    matches = optionsre.findall(data)
    ret = []
    for match in matches:
        if match not in ret:
            ret.append(match)
    ret.sort()

    return ret

def savelog(log, starttime, endtime):
    url = BASE_URL + PATH % (log, starttime, endtime, VERBOSE)
    fn = '%s_%s.log' % (log, starttime.strftime('%Y-%m-%d-%H-%M-%S'))
    fn = os.path.join(OUTPUT_DIR, fn)
    if REPLACE:
        mode = 'w'
    else:
        mode = 'a' # Append to any existing files so no data is deleted
    f = open(fn, mode)
    urlf = urllib.urlopen(url)
    for l in urlf:
        f.write(l)
    f.close()

def main():
    today = datetime.datetime.now()
    today = today.replace(hour=0, minute=0, second=0, microsecond=0)
    starttime = today - datetime.timedelta(days=1)
    endtime = today - datetime.timedelta(seconds=1)
    for log in availablelogs():
        print "Retrieving %s for %s" % (log, starttime)
        savelog(log, starttime, endtime)

if __name__ == '__main__':
    main()
