@echo off &setlocal

REM Put the path as root of where you want to find the files
REM leave echo in for first run so you can see what is going to be removed.
REM as soon as you remove 'echo' it will remove the files. BE CAREFUL WITH THIS AS IT WILL REMOVE FILES

for /f "delims=" %%i in ('dir /a-d /s /b C:\Temp\test\test.txt') do echo del "%%~i"
endlocal
