# Rename a user in AD after they have got married or changed their name.
#
# Must be run on a server with Exchange management tools

# Load up the exchange management shell
if (!(Get-PSSnapin | where {$_.Name -eq "Microsoft.Exchange.Management.PowerShell.E2010"})) {
    Write-Verbose "Loading the Exchange snapin"
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 -ErrorAction SilentlyContinue
    . $env:ExchangeInstallPath\bin\RemoteExchange.ps1
    Connect-ExchangeServer -auto -AllowClobber
} #else {
#    Write-Host "Exchange Powershell does not seem to be on this machine." -ForegroundColor red
#    Write-Host "You must run this script on a machine with Exchange powershell tools!!" -ForegroundColor red
#    exit
#}

# Import the active directory module
Import-Module ActiveDirectory

# Prompt for existing name
$existingFirstName  = Read-Host "Please enter the user's current First Name:"
$existingSurName    = Read-Host "Please enter the user's current Surname:"

# prompt for new name
$newFirstName       = Read-Host "Please enter the NEW First Name:"
$newSurName         = Read-Host "Please enter the NEW Surname:"

# Find the existing user
$currentFullName    = "$existingFirstName $existingSurName"
$currentUser        = Get-ADUser -properties * -filter { name -eq $currentFullName }

# Create the new user details
$newUsername        = ($newFirstName.Substring(0,1) + $newSurName).ToLower()
$newFullName        = "$newFirstName $newSurName"

if ([bool]($currentUser)) {
  $currentUser | Set-ADUser -DisplayName $newFullName `
  -EmailAddress "$newUsername@covenant.nsw.edu.au" `
  -GivenName $newFirstName `
  -Surname $newSurName `
  -SamAccountName $newUsername `
  -UserPrincipalName "$newUsername@covenant.nsw.edu.au"

  Rename-ADObject -Identity $currentUser.DistinguishedName -NewName $newFullName

  Start-Sleep -Seconds 10 # Wait for object to replicate

  Write-Host "Rename of AD object complete" -ForegroundColor green

  # Move the home directory to the new username
  $staffFolder = [string](($currentUser.HomeDirectory).Substring(0,$currentUser.HomeDirectory.IndexOf($currentUser.SamAccountName)))
  $currentUserDirectory = $staffFolder + $currentUser.SamAccountName
  #Write-Host $currentUserDirectory
  $newUserDirectory = $staffFolder + $newUsername
  $newHomeDirectory = ($currentUser.HomeDirectory).Replace($currentUser.SamAccountName,$newUsername)

  if ([bool](Test-Path $currentUserDirectory)) { # Test directory exists
    if ([bool]($currentUser.HomeDirectory)) { # Test the user has a HomeDirectory set
      Move-Item $currentUserDirectory $newUserDirectory

      Start-Sleep -Seconds 10 # wait for object to be available

      Get-ADUser $newUsername | Set-ADUser -HomeDirectory $newHomeDirectory
      Write-Host "Home directory have been moved to $newUserDirectory" -ForegroundColor green
      Write-Host "AD user has had their HomeDirectory property updated" -ForegroundColor green
    } else {
      Move-Item $currentUserDirectory $newUserDirectory
      Write-Host "AD user does not have a HomeDirectory set!!" -ForegroundColor yellow
      Write-Host "Moved folder to $newUserDirectory anyway..." -ForegroundColor green
    }

  } else {
    Write-Host "The home directory " + $currentUser.HomeDirectory + "does not exist!!" -ForegroundColor yellow
  }

} else {
  Write-Host "User does not exist in AD!!" ForegroundColor red
  exit
}

# Now edit the exchange mailbox
if ([bool](Get-Mailbox -Identity $currentUser.SamAccountName -ErrorAction SilentlyContinue)) {
  Get-Mailbox $currentUser.SamAccountName | Set-Mailbox -alias $newUsername
  Write-Host "Rename of mailbox complete" -ForegroundColor green
} else {
  Write-Host "User mailbox does not exist in Exchange!!" -ForegroundColor red
  exit
}
