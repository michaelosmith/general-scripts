@echo off
echo "Please enter your username:"
set /p Username=

:: Auth to TYCHICUS
net use \\TYCHICUS.covenant.nsw.edu.au /USER:CCS\%Username%

set _scripts=cscript %Windir%\System32\Printing_Admin_Scripts\en-US

::Add IT Printer
%_scripts%\prnmngr.vbs -ac -p "\\TYCHICUS.covenant.nsw.edu.au\A-Colour–HP-LaserJet-CP2025"

::Add A Block copier
%_scripts%\prnmngr.vbs -ac -p "\\TYCHICUS.covenant.nsw.edu.au\A Block - SHARP MX-M453N"
